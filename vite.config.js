const path = require('path')
import { defineConfig, loadEnv } from 'vite'


export default defineConfig(({ command, mode }) => {
  // Load env file based on `mode` in the current working directory.
  // Set the third parameter to '' to load all env regardless of the `VITE_` prefix.
  const env = loadEnv(mode, process.cwd(), '')
  console.log(mode);
  return {
    // vite config
    define: {
      __APP_ENV__: env.APP_ENV,
    },
    root: path.resolve(__dirname, 'src'),
    resolve: {
      alias: {
        '~bootstrap': path.resolve(__dirname, 'node_modules/bootstrap'),
      }
    },
    envDir: './',
    server: {
      port: 8080,
      hot: true
    }
  }
})
