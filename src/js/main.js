import GLightbox from 'glightbox'
import Swiper from 'swiper'
import { Popover } from 'bootstrap';
import AOS from 'aos';
import Typed from 'typed.js';
import PureCounter from "@srexi/purecounterjs";
import axios from "axios";
import Isotope from "isotope-layout";

import 'boxicons'
import 'swiper/css'
import '../scss/styles.scss'

const userId = '125549783@N03'

const select = (el, all = false) => {
  el = el.trim()
  if (all) {
    return [...document.querySelectorAll(el)]
  } else {
    return document.querySelector(el)
  }
}

const configurePortfolio = () => {
  const portfolioLightbox = GLightbox({
    selector: '.portfolio-lightbox'
  });
  let portfolioContainer = select('.portfolio-container');
  if (portfolioContainer) {

    let portfolioIsotope = new Isotope(portfolioContainer, {
      itemSelector: '.portfolio-item'
    });

    let portfolioFilters = select('#portfolio-flters li', true);

    portfolioFilters.forEach(box => {
      box.addEventListener('click', (e) => {
        e.preventDefault();
        portfolioFilters.forEach(function (el) {
          el.classList.remove('filter-active');
        });
        box.classList.add('filter-active');

        portfolioIsotope.arrange({
          filter: box.getAttribute('data-filter')
        });
        portfolioIsotope.on('arrangeComplete', function () {
          AOS.refresh()
        });
      });
    });

  }
}

const navButton = select('.mobile-nav-toggle')
navButton.addEventListener('click', (e) => {
  e.preventDefault();
  select('body').classList.toggle('mobile-nav-active')
  e.classList.toggle('bi-list')
  e.classList.toggle('bi-x')
});



const getRequestOptions = (method, aditionalParams = {}) => {
  return {
    method: 'GET',
    url: 'https://www.flickr.com/services/rest/',
    params: {
      api_key: '6aabd71a342b0a030a426ce62e6d0ec0',
      user_id: userId,
      format: 'json',
      nojsoncallback: '1',
      method,
      ...aditionalParams
    }
  };
}

const getAlbumData = async (id) => {
  const options = getRequestOptions('flickr.photosets.getPhotos', { photoset_id: id })
  try {
    const response = await axios.request(options);
    return Promise.resolve(response.data.photoset)
  } catch (error) {
    console.error(error);
    return []
  }
};

const createPortfolio = async (arrAlbums) => {
  const portfolioFilterUL = select('#portfolio-flters');
  const portfolioImages = select('#portfolio-container');
  const albumPhotos = await Promise.all(
    arrAlbums.map(async (album, index) => {
      const { id, title: albumTitle } = album
      const node = document.createElement("li")
      node.setAttribute("data-filter", `.filter-${index}`)
      const textnode = document.createTextNode(`Album ${index + 1}`)
      node.appendChild(textnode)
      portfolioFilterUL.appendChild(node);
      const { photo } = await getAlbumData(id) ?? []
      const images = photo.slice(0, 6).map((img) => {

        const template = select('#portfolio-item');
        let clone = template.content.cloneNode(true);
        clone.firstElementChild.classList.add(`filter-${index}`);
        let imgNode = clone.querySelectorAll("img");
        const url = `https://live.staticflickr.com/${img.server}/${img.id}_${img.secret}.jpg`
        imgNode[0].src = url

        const links = clone.querySelector(".portfolio-links")
        const firstLink = links.firstElementChild
        const lastLink = links.lastElementChild

        firstLink.href = url
        lastLink.href = `https://www.flickr.com/photos/${userId}/albums/${id}`
        firstLink.title = img.title

        portfolioImages.appendChild(clone);

        AOS.refresh()

        return {
          'href': url,
          'type': 'image',
          'title': img.title,
          'description': albumTitle._content,
        }
      })
      return images
    })
  );

  configurePortfolio()
}

const loadAlbums = () => {
  const options = getRequestOptions('flickr.photosets.getList')

  axios.request(options).then((response) => {
    const { photoset } = response.data.photosets
    createPortfolio(photoset)
  }).catch((error) => {
    console.error(error);
  });
}

window.addEventListener('load', () => {
  AOS.init({
    duration: 1000,
    easing: 'ease-in-out',
    once: true,
    mirror: false
  })

  loadAlbums()

});

// Create an example popover
document.querySelectorAll('[data-bs-toggle="popover"]')
  .forEach(popover => {
    new Popover(popover)
  })


new Swiper('.portfolio-details-slider', {
  speed: 400,
  loop: true,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false
  },
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable: true
  }
});

const typed = select('.typed')
if (typed) {
  let typed_strings = typed.getAttribute('data-typed-items')
  typed_strings = typed_strings.split(',')
  new Typed('.typed', {
    strings: typed_strings,
    loop: true,
    typeSpeed: 100,
    backSpeed: 50,
    backDelay: 2000
  });
}

new PureCounter();
